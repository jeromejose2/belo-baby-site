"use strict";
var tform;

$(document).ready(function() {
    init();
});

var website = {
    currentToggledIcon: 'default',
    oldToggleIcon: 'default',
    currentViewPort: false
};

function init() {

    website.validateNumber = function(event) {
        var key = window.event ? event.keyCode : event.which;
        if (!event.shiftKey) {
            if (event.keyCode == 8 || event.keyCode == 46 || event.keyCode == 37 || event.keyCode == 39) {
                return true;
            } else if (key < 48 || key > 57) {
                return false;
            } else return true;
        } else {
            return false;
        }
    };

    website.viewPort = function() {
        var e = window;
        var a = 'inner';

        //For Windows Mobile Devices (http://quirksmode.org/mobile/viewports2.html)
        if (!('innerWidth' in window)) {
            a = 'client';
            e = document.documentElement || document.body;
        }
        return { width: e[a + 'Width'], height: e[a + 'Height'] }
    };

    website.currentViewPort = new website.viewPort();

    website.jform = function() {
        /* TYPEFORM */
        $.fn.jtypeform = function(options) {
            var Self = this;
            var typeForm = {};
            var settings;
            settings = $.extend({
                onComplete: function(callback) {

                    console.log($('#typeform-footer').serializeArray());

                    $.ajax({
                        type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
                        url: 'process.php', // the url where we want to POST
                        data: formData, // our data object
                        dataType: 'json', // what type of data do we expect back from the server
                        encode: true
                    }).done(function(data) { // using the done promise callback

                        // log data to the console so we can see
                        console.log(data);

                        callback(); // Execute callback

                        // here we will handle errors and validation messages
                    });

                },
                duration: 300,
                typeForm: typeForm
            }, options);
            Self.prev = function() {
                typeForm.active = typeForm.list.children('.active');
                var prev = typeForm.active.prev('.item');
                var height = typeForm.children.not('.active').outerHeight();
                var index = prev.index();
                var ptop = (index * height) * -1;
                var isComplete = false;
                if (typeForm.active.is(':first-child')) {
                    return;
                }

                if (index == 4) {
                    $('.tform-next').html('<i class="fa fa-caret-down"></i>');
                    $('.tform-next').removeClass('send-active');
                }


                $('.error', typeForm.children).remove();
                typeForm.children.removeClass('active');
                prev.addClass('active');
                typeForm.list.css({ top: ptop });

                setTimeout(function() {
                    $('input, textarea, select', prev).focus();
                }, settings.duration);


            }
            Self.next = function() {
                typeForm.active = typeForm.list.children('.active');
                var next = typeForm.active.next('.item');
                var height = typeForm.children.not('.active').outerHeight();
                var index = next.index();
                var ptop = (index * height) * -1;
                var isComplete = false;
                $('.error', typeForm.children).remove();
                var input = $('input, textarea, select', typeForm.active);
                if (input.attr('data-error') && $.trim(input.val()) == '') {
                    input.focus();
                    typeForm.active.append('<div class="error">' + input.data('error') + '</div>');
                    return;
                }

                console.log(typeForm.children.length);
                console.log(index);

                if (typeForm.children.length - 1 === index) {
                    $('.tform-next').text('Send');
                    $('.tform-next').addClass('send-active');
                }


                if (typeForm.active.is(':last-child')) {

                    // Execute Self.clearform if Ajax form submission is complete.
                    settings.onComplete(Self.clearForm);
                    return;
                }



                var name = /^[a-zA-Z0-9\s]+$/;
                if (input.attr('name') == 'name') {
                    if (!name.test(input.val())) {
                        input.focus();
                        typeForm.active.append('<div class="error">Opps! You have entered invalid characters.</div>');
                        return false;
                    }
                }

                var email = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
                if (input.attr('name') == 'email') {
                    if (!email.test(input.val())) {
                        input.focus();
                        typeForm.active.append('<div class="error">Opps! You have entered an invalid email address</div>');
                        return false;
                    }
                }

                var contactPattern = /^\d+$/;

                console.log(contactPattern.test(input.val()))

                if (input.attr('name') == 'contact') {
                    var length = input.val();

                    if (input.val().length < 7) {
                        input.focus();
                        typeForm.active.append('<div class="error">Opps! You have entered an invalid contact number</div>');
                        return false;
                    }

                    if (!contactPattern.test(input.val())) {
                        input.focus();
                        typeForm.active.append('<div class="error">Opps! You have entered an invalid contact number</div>');
                        return false;
                    }

                }
                typeForm.children.removeClass('active');
                next.addClass('active');
                typeForm.list.css({ top: ptop });
                setTimeout(function() {
                    $('input, textarea, select', next).focus();
                }, settings.duration)
            }
            Self.clearForm = function() {
                typeForm.inputs.val('');
                typeForm.list.css({ top: 0 });
                typeForm.children.removeClass('active');
                typeForm.children.eq(0).addClass('active');
            }
            return this.each(function() {
                typeForm.elem = $(this);
                typeForm.list = typeForm.elem.children('ul');
                typeForm.children = typeForm.list.children();
                typeForm.inputs = $('input, textarea, select', typeForm.children);
                initialize(this);
            });

            function initialize() {
                if (typeForm.elem.hasClass('initialized')) return;
                typeForm.elem.addClass('initialized');
                typeForm.children.addClass('item')
                typeForm.children.eq(0).addClass('active');
                typeForm.list.css({
                    top: 0,
                    '-webkit-transition-duration': settings.duration + 'ms',
                    'transition-duration': settings.duration + 'ms',
                });
                typeForm.inputs.each(function() {
                    $(this)
                        .attr('tabindex', -1)
                        .keypress(function(e) {
                            if (getKeyCode(e) == 13 && !e.shiftKey) {
                                if (!typeForm.list.children('.active').is(':last-child')) {
                                    Self.next();
                                }
                            }
                        })
                })
                Self.clearForm();
            }

            function getKeyCode(evt) {
                return (evt.which) ? evt.which : evt.keyCode;
            }
        };

        function log(str) { console.log(str); }

        $(function() {
            tform = $('#typeform-footer').jtypeform();
        });
    };

    website.headerNavigation = function() {

        /* If on mobile devices */
        if (website.currentViewPort.width <= 1024 && Modernizr.touch) {
            $('.main-nav').on('click', 'li > a', function(e) {
                // e.preventDefault();

                //RESET  LI BG-COLOR of ALL MENU
                $('.main-nav').find('li').removeClass('active');

                //CHANGE LI BG-COLOR of CURRENT TOGGLED MENU
                $(this).parent().toggleClass('active');

                //ADD ACTIVE CLASS .NAV-INNER
                if ($(this).parent().find('.nav-inner')) {
                    $(this).parent().find('.nav-inner').toggleClass('active');
                }

                //UPDATE TOGGLED ICON
                if (website.oldToggleIcon === 'default') {
                    website.oldToggleIcon = $(this).find('span:last-child').attr('class');
                    $('.' + website.oldToggleIcon).toggleClass('active');
                    console.log('config once');
                } else {
                    website.currentToggledIcon = $(this).find('span:last-child').attr('class');
                    if (/active/.test(website.currentToggledIcon)) {
                        //active
                    } else {
                        //RESET OLD TOGGLE ICON
                        $('.' + website.oldToggleIcon).removeClass('active');
                        $('.' + website.currentToggledIcon).toggleClass('active');
                        website.oldToggleIcon = website.currentToggledIcon;
                    }
                }
            });
        } else {
            $('.main-nav').on('mouseover', 'li', function(e) {
                var $this = $(this),
                    $navInner = $this.children('.nav-inner');

                $this.addClass('active');
                $navInner.addClass('active');

            });

            $('.main-nav').on('mouseleave', 'li', function(e) {
                var $this = $(this),
                    $navInner = $this.children('.nav-inner');

                $this.removeClass('active');
                $navInner.removeClass('active');

            });
        }




        /* BURGER WHEN 1023px */
        $('.burger').on('click', function(e) {
            e.preventDefault();
            $(this).toggleClass('active');
            $('.main-nav').toggleClass('active');
        });

    };

    website.searchNavigation = function() {
        $('.btn-search').on('click', function() {
            $('.searchwrap').find('.form-item').toggleClass('active');
        })
    };


    website.bLazy = new Blazy({
        breakpoints: [{
            width: 480, // max-width
            src: 'data-src-mobile'
        }, {
            width: 768, // max-width
            src: 'data-src-tablet'
        }, {
            width: 1010, // max-width
            src: 'data-src-tablet-large'
        }]
    });

    website.ReviewPop = function() {
        var successPop = $.magnificPopup.instance,
            reviewPop = $.magnificPopup.instance;


        $('.btn-review').on('click', function(e) {

            reviewPop.open({
                items: {
                    src: 'popups/rating-form.html'
                },
                type: 'ajax',
                callbacks: {
                    ajaxContentAdded: function() {

                        $('#rate-form').barrating({
                            theme: 'fontawesome-stars'
                        });

                        $('.btn-submit').on('click', function(e) {
                            e.preventDefault();
                            console.log("hello");

                            $('.mfp-wrap').on('click', '.btn-success-close-pop', function(e) {
                                $.magnificPopup.close();
                            });

                            successPop.open.call(successPop, {
                                items: {
                                    src: 'popups/review-success.html'
                                },
                                type: 'ajax'
                            });


                            return false;
                        });
                    }
                }
            });

            e.preventDefault();
        });

    };

    website.searchTrigger = function() {
        var searchPop = $.magnificPopup.instance;

        $('.search-pop-trigger').on('click', 'a', function(e) {
            e.preventDefault();

            searchPop.open({
                items: {
                    src: 'popups/search.html'
                },
                type: 'ajax',
                callbacks: {
                    ajaxContentAdded: function() {}
                }
            });

        });
    };

    website.tableTrigger = function() {
        if (website.currentViewPort.width <= 480 && Modernizr.touch) {

            $('.toggle-comparison').on('click', '.trigger-col', function(e) {
                e.preventDefault();
                var $parent = $(e.delegateTarget),
                    $this = $(this);

                if ($this.hasClass('b-current')) {

                    $parent.find('.b-current').hide();
                    $parent.find('.b-other').show();

                } else if ($this.hasClass('b-other')) {

                    $parent.find('.b-current').show();
                    $parent.find('.b-other').hide();

                }

            });

        }

    };

    website.triggerSliderGuarantee = function() {
        if (website.currentViewPort.width <= 767 && Modernizr.touch) {
            $("#guarantee-slider-mobile").owlCarousel({
                autoPlay: false, //Set AutoPlay to 3 seconds
                items: 1,
                itemsDesktop: [1024, 1],
                itemsDesktopSmall: [1024, 1],
                itemsTablet: [768, 1],
                itemsTabletSmall: false,
                itemsMobile: [479, 1],

                navigation: false,
                pagination: true
            });
        }
    };

    website.initHomePageSlider = function() {
        $('#fullpage-carousel').owlCarousel({
            autoPlay: false,
            items: 1,
            itemsDesktop: [1024, 1],
            itemsDesktopSmall: [1024, 1],
            itemsTablet: [768, 1],
            itemsTabletSmall: false,
            itemsMobile: [479, 1],

            navigation: false,
            pagination: true
        });
    };

    website.initInnerArticle = function(){
        $('#carousel-might-like').owlCarousel({
            autoPlay: false,
            items: 4,
            itemsDesktop: [1024, 4],
            itemsDesktopSmall: [1024, 3],
            itemsTablet: [768, 3],
            itemsTabletSmall: false,
            itemsMobile: [479, 2],
            // navigation: false,
            pagination: true
        });
    }


    console.log(website.currentViewPort);
    console.log(Modernizr.touch);

    website.headerNavigation();
    website.searchNavigation();
    website.tableTrigger();
    website.triggerSliderGuarantee();
    website.ReviewPop();
    website.jform();
    website.searchTrigger();
    website.initHomePageSlider();
    website.initInnerArticle();
}
